#include "entity.h"

namespace kokain
{

namespace core
{

qint64 entity::s_id_counter = Q_INT64_C(0);

bool operator==(const entity& e1, const entity& e2)
{
    return e1.id() == e2.id();
}

bool operator<(const entity& e1, const entity& e2)
{
    return e1.id() < e2.id();
}

// should be called from outside general intializer function
void entity::initialize_id_counter_()
{
    // TODO: read from db the last id of the object and set
}

entity::entity()
    : m_id(s_id_counter++)
{
}

entity::~entity()
{

}

void entity::set_id(qint64 id)
{
    if (id >= 0) {
        m_id = id;
    }
}

qint64 entity::id() const
{
    return m_id;
}

qint64 entity::entity_id() const
{
    return id();
}

entity* entity::current_entity()
{
    return this;
}

//user* entity::default_author()
//{
//    return NULL;
//}

//void entity::set_tags(const std::vector<tag *> &tg)
//{
//    if (!tg.empty()) {
//        m_tags.clear();
//        m_tags = tg;
//    }
//}

//void entity::add_tag(tag *t)
//{
//    Q_ASSERT(0 != t);
//    m_tags.push_back(t);
//}

//void entity::remove_tag(tag *t)
//{
//    std::vector<tag*>::iterator it = m_tags.begin();
//    for ( ; it != m_tags.end(); ++it)
//    {
//        const tag* tmp = *it;
//        if (*tmp == *t) { // tag::operator==
//            m_tags.erase(it);
//            break;
//        }
//    }
//}

//bool entity::has_tag(tag *t) const
//{
//    std::vector<tag*>::const_iterator it = m_tags.begin();
//    for ( ; it != m_tags.end(); ++it)
//    {
//        const tag* tmp = *it;
//        if (*tmp == *t) {
//            return true;
//        }
//    }
//    return false;
//}

//std::vector<tag*> entity::tags()
//{
//    return m_tags;
//}

//void entity::set_references(const std::vector<reference *> &ref)
//{
//    if (!ref.empty()) {
//        m_references.clear();
//        m_references = ref;
//    }
//}

//void entity::add_reference(reference *r)
//{
//    Q_ASSERT(0 != r);
//    m_references.push_back(r);
//}

//void entity::remove_reference(reference *r)
//{
//    Q_ASSERT(0 != r);
//    std::vector<reference*>::iterator it = m_references.begin();
//    for ( ; it != m_references.end(); ++it) {
//        const reference* tmp = *it;
//        if (*tmp == *r) {
//            m_references.erase(it);
//            break;
//        }
//    }
//}

//bool entity::has_reference(reference *r) const
//{
//    Q_ASSERT(0 != r);
//    std::vector<reference*>::const_iterator it = m_references.begin();
//    for ( ; it != m_references.end(); ++it)
//    {
//        const reference* tmp = *it;
//        if (*tmp == *r) {
//            return true;
//        }
//    }
//    return false;
//}

//std::vector<reference*> entity::references()
//{
//    return m_references;
//}

//QString entity::serialize_to_json() const
//{
//    QJsonObject j_obj;

//    j_obj.insert("id", id());

//    QJsonDocument j_doc(j_obj);
//    return QString(j_doc.toJson());
//}

//void entity::init_from_json(const QString& serialized)
//{
//    QJsonDocument j_doc = QJsonDocument::fromJson(QByteArray(serialized.toStdString().c_str()));
//    QJsonObject j_obj = j_doc.object();

//    m_id = quint64(j_obj.value("id").toInt());

//    return;
//}

//QString entity::object_type() const
//{
//    return "entitiy";
//}

} // namespace core

} // namespace kokain
