QT       += core
QT += sql

QT       -= gui
QT       += qml

HEADERS += \
    calendar_piece.h \
    calendar.h \
    dialog_box.h \
    event.h \
    events_list.h \
    more_events.h \
    year_month.h \
    year.h \
    month.h \
    event_start_end.h \
    condition.h \
    db_manager.h \
    query.h \
    entity.h \
    current_timezone.h \
    event_manager.h \
    timezone.h \
    label_edit.h \
    more_labels.h \
    multiple_labels.h \
    user_avatar_widget.h \
    user_data.h \
    user_dialog_box.h \
    flowlayout.h \
    dragwidget.h \
    test.h

SOURCES += \
    calendar_piece.cpp \
    calendar.cpp \
    main.cpp \
    dialog_box.cpp \
    event.cpp \
    events_list.cpp \
    more_events.cpp \
    year_month.cpp \
    year.cpp \
    month.cpp \
    event_start_end.cpp \
    query.cpp \
    db_manager.cpp \
    condition.cpp \
    entity.cpp \
    current_timezone.cpp \
    event_manager.cpp \
    timezone.cpp \
    user_dialog_box.cpp \
    user_data.cpp \
    dragwidget.cpp \
    flowlayout.cpp \
    label_edit.cpp \
    more_labels.cpp \
    multiple_labels.cpp \
    test.cpp \
    user_avatar_widget.cpp

QT += widgets
