#ifndef USER_DATA_H
#define USER_DATA_H

#include <QtWidgets>

#include "label_edit.h"
#include "multiple_labels.h"

class QVBoxLayout;
class QHBoxLayout;
class multiple_labels;
class QLabel;
class QPushButton;


class user_data:public QFrame
{
  Q_OBJECT

public:
    explicit  user_data(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    QVBoxLayout* m_main;
    QHBoxLayout* m_curr;
    multiple_labels* m_current;
    QLabel* m_curr_name;
    QPushButton* m_back;
    QPushButton* m_calendar_show;
    QVector<multiple_labels*> m_all_options;

public:
    void add_data(QString name, bool flag);
    QVector<multiple_labels *> &get_all_options();
    void set_data(QVector<QVector<QString>>);

public slots:
    void back_clicked();
    void data_changed();
    void calendar_showed();

signals:
    void data_back_clicked();
    void on_data_changed();
    void on_calendar_showed();
};

#endif // USER_DATA_H
