#ifndef QUERY_H
#define QUERY_H

#include <QVector>
#include <QString>

#include "condition.h"

namespace kokain
{
    namespace db
    {
    //query_type
        enum query_type : short
        {
            CREATE,
            INSERT,
            SELECT,
            REMOVE,
            UPDATE,
            OPTIMIZE,
            DROP
        };

        class query
        {
        public:
            query(query_type type, const QString& tbl_name);

            QString get_result();

            void setId(const int id);
            int getId();

            void setValue(const QString& val);
            QString getValue();

            void setCondition(const condition& cond);
            condition getCondition();

            query_type getQueryType();
            QString getTableName();

        private:
            query(const query& obj);
            query& operator=(const query& obj);

        private:
            QString m_table_name;
            int m_id;

            QString m_value;
            condition m_cond;
            query_type m_type;
            QString m_result;
        };//class query
    }//namespace db
}//namespace kokain
#endif // QUERY_H
