#include "year.h"


year::year(QWidget *parent):QLabel(parent)
{
    initilize_();
}

void year::initilize_()
{

    create_widgets_();
    make_connections_();

    setStyleSheet("border: 1px solid");
}

void year::create_widgets_()
{
    QFont font = this->font();
    font.setPointSize(10);
    setFont(font);
    setFixedSize(55,20);
    setText(QString::number(QDate::currentDate().year()));

    m_text_edit = new QLineEdit(this);
    m_text_edit->setValidator( new QIntValidator(1800, 8000, this) );
    m_text_edit->setText(QString::number(QDate::currentDate().year()));


    m_text_edit->setFixedSize(55,20);
    m_text_edit->close();
}



void year::make_connections_()
{
    connect(m_text_edit, SIGNAL(returnPressed()), this, SLOT(clicked()));
}


void year::mouseDoubleClickEvent(QMouseEvent *event)
{
      if (event -> button() == Qt::LeftButton) {
          m_text_edit->setText(text());
          m_text_edit->show();
      }
}



void year::clicked()
{
    setText(m_text_edit->text());
    m_text_edit->close();
    emit date_changed();
}

int year::get_year()
{
    return m_text_edit->text().toInt();
}

void year::set_year(int curr_year)
{
    setText(QString::number(curr_year));
    m_text_edit->setText(QString::number(curr_year));
}




