#include "dialog_box.h"

dialog_box::dialog_box(QWidget *parent):QDialog(parent)
{
    initilize_();
}

void dialog_box::initilize_()
{
    create_widgets_();
    setup_layout_();
    make_connections_();
    setFixedSize(475,220);
    setModal(true);
}

void dialog_box::create_widgets_()
{
    m_event_name = new QLabel("Event name",this);
    m_event_color = new QLabel("Color", this);

    m_event_name_edit = new QLineEdit(this);
    m_event_color_edit = new QLineEdit(this);

    m_event_start_end = new event_start_end;

    m_ok = new QPushButton("Ok",this);
    m_cancel = new QPushButton("Cancel", this);
    m_discard = new QPushButton("Discard", this);

    m_event_timezone = new event_timezone;

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
}

void dialog_box::setup_layout_()
{
    QHBoxLayout* buttons = new QHBoxLayout;

    buttons->addWidget(m_ok);
    buttons->addWidget(m_cancel);
    buttons->addWidget(m_discard);

    QVBoxLayout* task_parameters = new QVBoxLayout;

    task_parameters->addWidget(m_event_name);
    task_parameters->addWidget(m_event_color);

    QVBoxLayout* task_parameters_edit = new QVBoxLayout;

    QHBoxLayout* color = new QHBoxLayout;
    color->addWidget(m_event_color_edit);

    task_parameters_edit->addWidget(m_event_name_edit);
    task_parameters_edit->addLayout(color);

    QHBoxLayout* all = new QHBoxLayout;

    all->addLayout(task_parameters);
    all->addLayout(task_parameters_edit);

    QVBoxLayout* main = new QVBoxLayout;
    main->addLayout(all);
    main->addWidget(m_event_timezone);
    main->addWidget(m_event_start_end);
    main->addLayout(buttons);

    setLayout(main);
}


void dialog_box::make_connections_()
{
    connect(m_ok,SIGNAL(clicked()),this,SLOT(my_accept()));
    connect(m_cancel,SIGNAL(clicked()),this,SLOT(close()));
    connect(m_discard, SIGNAL(clicked()), this, SLOT(discarded()));
}



QString dialog_box::get_event_name()
{
    return m_event_name_edit->text();
}



QString dialog_box::get_color()
{
    return m_event_color_edit->text();
}

void dialog_box::my_accept()
{
    emit ok_clicked();
    this->close();
}

void dialog_box::set_event_name(QString name)
{
    m_event_name_edit->setText(name);
}

void dialog_box::set_event_color(QString color)
{
    m_event_color_edit->setText(color);
}

void dialog_box::discarded()
{
    emit discard_clicked();
    this->close();
}

void dialog_box::set_start_date(QDateTime start)
{
    m_event_start_end->set_start_date(start);
}


void dialog_box::set_end_date(QDateTime end)
{
    m_event_start_end->set_end_date(end);
}

QDateTime dialog_box::get_end_date()
{
   return  m_event_start_end->get_end_date();
}

QDateTime dialog_box::get_start_date()
{
   return  m_event_start_end->get_start_date();
}

QTimeZone dialog_box::get_timezone()
{
    return m_event_timezone->get_time_zone();
}

void dialog_box::timezone(QList<current_timezone> list)
{
    m_event_timezone->timezones(list);
}


