#include "db_manager.h"
#include "query.h"

#include <iostream>
#include <QtSql/qsql.h>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QVariant>
#include <QtSql/QSqlError>


namespace  kokain {

    namespace  db {

        db_manager* db_manager::s_instance;

        db_manager::db_manager(const QString& db_file_name)
        {
            this->m_sdb = &QSqlDatabase::addDatabase("QSQLITE");
            this->m_sdb->setDatabaseName(db_file_name);
            this->m_sdb->open();
            counter = 0;
        }

        void db_manager::check_and_optimize(query& q)
        {
            if(q.getQueryType() == query_type::INSERT ||
                    q.getQueryType() == query_type::UPDATE ||
                    q.getQueryType() == query_type::REMOVE)
            {
                ++counter;
                if (counter % 100 == 0)
                {
                    query* q1 = new query(query_type::OPTIMIZE, q.getTableName());
                    QSqlQuery optquery(q1->get_result());
                }
            }
        }
        const QSqlDatabase* db_manager::getDb()
        {
            return this->m_sdb;
        }
        QSqlQuery db_manager::execute(query& q)
        {
            QSqlQuery sqlquery(q.get_result());

            check_and_optimize(q);
            return sqlquery;
        }

        db_manager::~db_manager()
        {
            delete this->m_sdb;
            this->m_sdb = nullptr;
        }
    }//namespace db
}//namespace kokain

