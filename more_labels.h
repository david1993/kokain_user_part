#ifndef MORE_LABELS_H
#define MORE_LABELS_H

#include<QtWidgets>

#include "label_edit.h"

class QVBoxLayout;

class more_labels:public QWidget
{
  Q_OBJECT

public:
     explicit more_labels(QWidget* parent = 0);

private:
    QQueue<label*>* m_labels;
    QVBoxLayout* m_main_layout;

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();

public:
    void add_label(label* curr_label);
    void remove_curr();
    label* get_label();
    label* curr_label(int i);
    bool is_empty();
    int count();
};

#endif // MORE_LABELS_H
