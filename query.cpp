#include "query.h"
#include <iostream>

namespace kokain
{
    namespace db
    {
        QString query::get_result()
        {
            if (m_type == query_type::INSERT)
            {
                m_result += "INSERT INTO " + m_table_name + "(content)";

                m_result += (" VALUES('" + m_value + "'" + ");");

                return m_result;
            }

            if (m_type == query_type::SELECT)
            {
                m_result += "SELECT * ";

                m_result += ("FROM " + m_table_name + " WHERE " + m_cond.get_value() + ";");
                return m_result;
            }

            if (m_type == query_type::REMOVE)
            {
                m_result += "DELETE FROM " + m_table_name + " WHERE " + m_cond.get_value() + ";";
                return m_result;
            }

            if (m_type == query_type::UPDATE)
            {
                m_result += "UPDATE " + m_table_name + " SET ";

                m_result += ("content = '" + m_value+ "' ");
                m_result += ("WHERE " + m_cond.get_value() + ";");
                return m_result;
            }

            if (m_type == query_type::CREATE)
            {
                m_result += "CREATE VIRTUAL TABLE " +
                        m_table_name + " USING fts3(content);";
                return m_result;
            }
            if (m_type == query_type::DROP)
            {
                m_result += "DROP TABLE " + m_table_name + ";";
                return m_result;
            }

            if (m_type == query_type::OPTIMIZE)
            {
                m_result += "INSERT INTO" + m_table_name + "(" + m_table_name + ")" + " VALUES('optimize');";
                return m_result;
            }

            return "";
        }

        query::query(query_type type, const QString& tbl_name)
            :m_type(type),
            m_table_name(tbl_name),
            m_result("")
        {}

        void query::setId(const int id)
        {
            this->m_id = id;
        }

        int query::getId()
        {
            return this->m_id;
        }

        void query::setValue(const QString& val)
        {
            this->m_value = val;
        }

        QString query::getValue()
        {
            return this->m_value;
        }

        void query::setCondition(const condition &cond)
        {
            this->m_cond = cond;
        }

        condition query::getCondition()
        {
            return this->m_cond;
        }

        query_type query::getQueryType()
        {
            return this->m_type;
        }

        QString query::getTableName()
        {
            return this->m_table_name;
        }
    }//namespace db
}//namespace kokain

