#ifndef USER_AVATAR_WIDGET_H
#define USER_AVATAR_WIDGET_H

#include <QtWidgets>

#include "user_data.h"
#include "entity.h"
#include "calendar.h"

class QLabel;
class QPoint;
class QString;
class user_data;

class user_avatar_widget : public QWidget, public kokain::core::entity
{
   Q_OBJECT
public:
    explicit user_avatar_widget(QWidget* widget = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();


private:
    QLabel* m_picture;
    QLabel* m_name;
    QPoint m_ptDragPos;
    QString m_indicator;
    user_data* m_data;
    calendar* m_user_calendar;

public:
    void set_name(QString);
    void set_picture(QPixmap picture);
    void set_pos(QPoint pos);


    QPoint get_pos();
    QString get_indicator();
    void set_indicator(QString ind);
    QString get_name() const;
    QPixmap get_picture() const;
    user_data* get_m_data();


protected:
    void startDrag();
    virtual bool eventFilter(QObject *target, QEvent *event);

public:
    virtual QString serialize_to_json() const;
    virtual void deserialize_from_json(const QString &serialized);
    virtual QString object_type() const;

public slots:
    void user_data_changed();
    void calendar_showed();


signals:
    void data_showed();
    void on_user_data_changed();
};


#endif // USER_AVATAR_WIDGET_H
