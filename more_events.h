#ifndef MORE_EVENTS_H
#define MORE_EVENTS_H


#include<QtWidgets>

#include "event.h"

class QVBoxLayout;

class more_events:public QWidget
{
  Q_OBJECT

public:
     explicit more_events(QWidget* parent = 0);

private:
    QVBoxLayout* m_main_layout;
    QList<cal_event*> m_events_list;
    bool m_new_event;
    cal_event* m_cur_event;
    QVector<cal_event*> m_deleteable;
    QDateTime m_date_time;
    QList<current_timezone> m_all_timezones;

private:
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

public:
    void add_event(cal_event* curr_event);
    void remove_curr();
    cal_event* get_event();
    cal_event* curr_event(int i);
    bool is_empty();
    int count();
    void set_flag(bool);
    void event_created_signal();
    void set_list(QList<cal_event*>);
    QList<cal_event *> get_list();
    void set_date_time(QDateTime);
    void timezone(QList<current_timezone>);

public slots:
    void closed();
    void closed_other();

signals:
    void event_created();
    void on_closed();
    void fiinsh_signal();
};


#endif // MORE_EVENTS_H
