#ifndef EVENT_START_END_H
#define EVENT_START_END_H

#include <QtWidgets>

#include "event_start_end.h"
#include "year.h"

class event_start_end : public QWidget
{
    Q_OBJECT

public:
    explicit event_start_end(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void setup_layouts_();
    void make_connections_();

public:
    int get_start_year();
    int get_start_month();
    QTime get_start_time();
    int get_end_year();
    int get_end_month();
    QTime get_end_time();
    QDateTime get_start_date();
    QDateTime get_end_date();
    void set_start_date(QDateTime);
    void set_end_date(QDateTime);

private:
    QDateTimeEdit* m_start_date;
    QDateTimeEdit* m_end_date;
};

#endif // EVENT_START_END_H
