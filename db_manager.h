#ifndef DB_MANAGER_H
#define DB_MANAGER_H


#include <QtSql/qsql.h>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QString>
#include <QList>
#include "query.h"

namespace kokain
{
    namespace db
    {
        class db_manager
        {
        private:
            db_manager(const QString& db_file_name);
            db_manager(const db_manager& obj);
            db_manager& operator=(const db_manager& obj);
            void check_and_optimize(query& q);

            ~db_manager();
            QSqlDatabase* m_sdb;
            static db_manager* s_instance;
            int counter;
        public:
            QSqlQuery execute(query& q);
            const QSqlDatabase* getDb();

            static db_manager* db_manager::get_instance(const QString& db_file_name)
            {
                s_instance = new db_manager(db_file_name);
                return s_instance;
            }
            static void db_manager::remove_instance()
            {
                delete s_instance;
                s_instance = nullptr;
            }
        };
    }//namespace db
}//namespace kokain
#endif // DB_MANAGER_H
