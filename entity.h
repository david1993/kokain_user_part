#ifndef ENTITY_H
#define ENTITY_H

#include <vector>

#include <QtGlobal>
#include <QString>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>


namespace kokain
{

namespace core
{

class entity;

bool operator==(const entity& e1, const entity& e2);

bool operator<(const entity& e1, const entity& e2);

//class tag;
//class reference;

class entity
{
public:
    entity();
    virtual ~entity();

    /// @brief ID
public:
    void set_id(qint64); // NOTE: should be used carefully!!
    qint64 id() const;

    /// @brief Overrided member-functions (from shareable)
public:
    virtual qint64 entity_id() const;
    virtual entity* current_entity();

//protected:
//    virtual user* default_author();

private:
    qint64 m_id;

    /// @brief Tags
//public:
//    virtual void set_tags(const std::vector<tag*>& tg);
//    virtual void add_tag(tag* t);
//    virtual void remove_tag(tag* t);
//    virtual bool has_tag(tag* t) const;
//    virtual std::vector<tag*> tags();

//private:
//    std::vector<tag*> m_tags;

    /// @brief References
//public:
//    virtual void set_references(const std::vector<reference*>& ref);
//    virtual void add_reference(reference* r);
//    virtual void remove_reference(reference* r);
//    virtual bool has_reference(reference* r) const;
//    virtual std::vector<reference*> references();
//private:
//    std::vector<reference*> m_references;

    /// @brief Pure virtual functions referred to json-like serializations
public:
    virtual QString serialize_to_json() const = 0;
    virtual void deserialize_from_json(const QString& serialized) = 0;
    virtual QString object_type() const = 0;

private:
    static void initialize_id_counter_();

private:
    static qint64 s_id_counter;

private:
    entity(const entity&);
    entity& operator=(const entity&);

}; // class entity

} // namespace core

} // namespace kokain

#endif // ENTITY_H
