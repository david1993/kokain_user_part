#include "user_avatar_widget.h"
#include "dragwidget.h"
#include "flowlayout.h"

#include "db_manager.h"
#include "condition.h"
#include "query.h"
using namespace kokain::core;

#include <QtWidgets>

DragWidget::DragWidget(QWidget *parent)
    : QListWidget(parent)
{
    initilize_();
    setAcceptDrops(true);
}


void DragWidget::initilize_()
{
    setup_layout_();
}

void DragWidget::setup_layout_()
{
    flowLayout = new FlowLayout(this);
    setLayout(flowLayout);
}


void DragWidget::set_user(user_avatar_widget *user)
{
    user->set_indicator("on_workers");
    flowLayout->addWidget(user);
}

void DragWidget::dropEvent(QDropEvent *event)
{

    if (event->mimeData()->hasFormat("application/x-fridgemagnet")) {

        const QMimeData *mime = event->mimeData();

        QByteArray itemData = mime->data("application/x-fridgemagnet");
        QDataStream dataStream(&itemData, QIODevice::ReadOnly);

        QString text;
        QString ind;
        QPoint offset;
        QPixmap pixmap;

        qint64 id;
        dataStream >> id >> offset;

        QSqlQuery res1("SELECT * FROM users");
        res1.seek(-1);
        user_avatar_widget* cur;
        while (res1.next()) {
            cur = new user_avatar_widget;
            cur->deserialize_from_json(res1.value(0).toString());
            if(cur->id() != id)
                delete cur;
            else
                break;
        }


        flowLayout->addWidget(cur);
        cur->show();

        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void DragWidget::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-fridgemagnet")) {
        if (event->source() == this) {
            event->setDropAction(Qt::MoveAction);
            event->accept();
        } else {
            event->acceptProposedAction();
        }
    } else {
        event->ignore();
    }
}

void DragWidget::dragMoveEvent(QDragMoveEvent *event)
{
    if (event->mimeData()->hasFormat("application/x-fridgemagnet")) {
        event->setDropAction(Qt::MoveAction);
        event->accept();
    } else {
        event->ignore();
    }
}






