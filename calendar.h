#ifndef CALENDAR_H
#define CALENDAR_H

#include <QtWidgets>

#include "calendar_piece.h"
#include "year_month.h"

class event_manager;

class calendar : public QWidget
{
    Q_OBJECT
private:
    QLabel* sun;
    QLabel* mon;
    QLabel* tue;
    QLabel* wed;
    QLabel* thu;
    QLabel* fri;
    QLabel* sut;
    QVBoxLayout* m_vertical;
    QHBoxLayout* m_horizontal;
    QHBoxLayout* m_horizontal0;
    QHBoxLayout* m_horizontal1;
    QHBoxLayout* m_horizontal2;
    QHBoxLayout* m_horizontal3;
    QHBoxLayout* m_horizontal4;
    QHBoxLayout* m_horizontal5;
    QHBoxLayout* m_horizontal6;
    calendar_piece** cal_pieces;
    calendar_piece** cal_pieces_zamen;
    calendar_piece* m_piece;
    year_month* m_year_month;
    QWidget* m_calendar;
    QWidget* m_calendar1;
    QWidget* m_calendar2;
    QVBoxLayout* main;
    qint64 m_events_ids;
    db_manager* manager;
    query* q;
    QList<QWidget*> m_calendar_list;
    QVector<cal_event*> m_current_curr_events;
    qint64 flag;
    event_manager* m_event_manager;
    QList<current_timezone> m_all_timezones;
public:
    calendar();
    void delete_events();
    static bool shouldRemove(current_timezone first, current_timezone second)
    {
        return ( first.get_name() < second.get_name() );
    }

private:
    void resizeEvent(QResizeEvent* e);

private slots:
    void on_date_changed();

};

#endif // CALENDAR_H
