#ifndef LABEL_EDIT_H
#define LABEL_EDIT_H

#include <QtWidgets>

class QLabel;
class QLineEdit;
class QPixmap;


class label:public QLabel
{
    Q_OBJECT

public:
    explicit label(QWidget* parent = 0);

private:
    void initilize_();
    void create_widgets_();
    void make_connections_();

private:
    const static qint32 close_pos;

private:
    QLabel* m_label;
    QLineEdit* m_text_edit;
    QPixmap* m_delete;
    QLabel* m_image;
    QLabel* m_link;
    bool m_clicked_close;
    bool m_is_link;
    bool m_first_enter;

protected:
    void mouseDoubleClickEvent ( QMouseEvent * event );
    void mouseReleaseEvent ( QMouseEvent * event );
    void mousePressEvent ( QMouseEvent * event );

public:
    void set_is_link(bool flag);
    QString get_label();
    void set_label(QString line);
    void m_link_show();

public slots:
    void clicked();

signals:
    void closed();
    void enter_click();
    void other_enter_clicked();
};


#endif // LABEL_EDIT_H
