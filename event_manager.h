#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include <QList>
#include <QDateTime>
#include <QtWidgets>
#include "entity.h"
#include "event.h"

class cal_event;

class event_manager
{

private:
    QList<cal_event*> m_events;

public:
    event_manager();
    QList<cal_event*> get_events();
    QList<cal_event*> get_events_for_day(QDateTime);
    void set_events(QList<cal_event*>);
    //void update();
    void remove_all();
    void add_event(cal_event*);
    void remove_at(qint64 id);
};


#endif // EVENT_MANAGER_H
