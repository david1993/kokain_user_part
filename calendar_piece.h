#ifndef CALENDAR_PIECE_H
#define CALENDAR_PIECE_H

#include <QtWidgets>
#include <QVector>

#include "dialog_box.h"
#include "event.h"
#include "more_events.h"
#include "db_manager.h"
#include "condition.h"
#include "query.h"
#include "event_manager.h"
using namespace kokain::db;

class QLabel;



class calendar_piece:public QLabel
{
    Q_OBJECT

public:
    explicit calendar_piece(QWidget* parent = 0);

private:
    bool flag;
    void initilize_();
    void create_widgets_();
    void setup_layout_();
    void make_connections_();

private:
    query* q;
    db_manager* manager;
    event_manager* m_event_manager;
    QSqlQuery res;
    QString m_date_day;
    QString m_line_color;
    QString m_round_color;
    dialog_box* m_event;
    QList<cal_event*> m_events_list;
    more_events* m_more_events;
    cal_event* cur_event;
    bool m_mouse_move;
    bool m_not_in_this_month;
    qint64* m_events_ids;
    QDateTime m_date_time;
    QList<current_timezone> m_all_timezones;

public:
    void set_date_day(QString&);
    void set_line_color(QString&);
    void set_round_color(QString&);
    void set_list(QList<cal_event*>);
    void set_is_on_in_this_month(bool);
    //void add_event(cal_event* eventik);
    QString get_date_day();
    QString get_line_color();
    QString get_round_color();
    void set_db_manager(db_manager*);
    void set_events_ids(qint64&);
    void delete_events();
    void set_date_time(QDateTime);
    void set_event_manager(event_manager*);
    void timezones(QList<current_timezone>);

private:
    void paintEvent(QPaintEvent* event);
    void enterEvent(QEvent* e);
    void leaveEvent(QEvent* e);
    void mousePressEvent(QMouseEvent* e);

public slots:
    void event_created();
    void closed();
    void more_closed();
signals:
    void on_closed();
    void ended();
};

#endif // CALENDAR_PIECE_H
